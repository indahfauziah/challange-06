"use strict";
const {Model} = require("sequelize");
module.exports = (sequelize, DataTypes) => {
    class Activity_Logs extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
            models.Activity_Logs.belongsTo(models.Users, {
                foreignKey: "id_user",
            });
            models.Activity_Logs.belongsTo(models.Cars, {
                foreignKey: "id_car",
            });
        }
    }
    Activity_Logs.init(
        {
            id_car: DataTypes.INTEGER,
            id_user: DataTypes.INTEGER,
            activity: DataTypes.STRING,
        },
        {
            sequelize,
            modelName: "Activity_Logs",
        }
    );
    return Activity_Logs;
};
