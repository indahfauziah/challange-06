"use strict";
const {Model} = require("sequelize");
module.exports = (sequelize, DataTypes) => {
    class Users extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
            models.Users.belongsTo(models.Type_Users, {
                foreignKey: "id_type",
            });
            models.Users.hasMany(models.Activity_Logs, {
                foreignKey: "id_type",
            });
        }
    }
    Users.init(
        {
            id_type: DataTypes.INTEGER,
            email_user: DataTypes.STRING,
            password_user: DataTypes.STRING,
        },
        {
            sequelize,
            modelName: "Users",
        }
    );
    return Users;
};
