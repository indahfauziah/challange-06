const {Type_Cars} = require("../models");

module.exports = {
    create(createArgs) {
        return Type_Cars.create(createArgs);
    },

    update(id, updateArgs) {
        return Type_Cars.update(updateArgs, {
            where: {
                id,
            },
        });
    },

    delete(id) {
        return Type_Cars.destroy({where: {id}});
    },

    find(id) {
        return Type_Cars.findByPk(id);
    },

    findAll() {
        return Type_Cars.findAll();
    },
};
