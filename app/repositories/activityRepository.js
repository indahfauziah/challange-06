const {Activity_Logs} = require("../models");

module.exports = {
    create(createArgs) {
        return Activity_Logs.create(createArgs);
    },

    update(id, updateArgs) {
        return Activity_Logs.update(updateArgs, {
            where: {
                id,
            },
        });
    },

    delete(id) {
        return Activity_Logs.destroy({where: {id}});
    },

    find(id) {
        return Activity_Logs.findByPk(id);
    },

    findAll() {
        return Activity_Logs.findAll();
    },
};
