// const authControllersSuper = require("../controllers/api/v1/authControllersSuper");
const authRepositorySuper = require("../repositories/authRepositorySuper")

module.exports = {
  // register(requestBody) {
  //   return postRepository.create(requestBody);
  // },
  get(email) {
    return authRepositorySuper.find(email);
  },
  getPk(id) {
    return authRepositorySuper.findPk(id);
  },
};