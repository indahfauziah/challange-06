const carsService = require("../../../services/carsService");
const activityService = require("../../../services/activityService");

module.exports = {
    async listCar(req, res) {
        if (req.Users.id_type !== "") {
            carsService
                .list()
                .then(({data, count}) => {
                    res.status(200).json({
                        status: "OK",
                        data: {Cars: data},
                    });
                })
                .catch((err) => {
                    res.status(400).json({
                        status: "FAIL",
                        message: err.message,
                    });
                });
        } else {
            res.status(401).json({
                status: "FAIL",
                message: "Unauthorized",
            });
        }
    },

    async listDeletedCars(req, res) {
        if (req.Users.id_type === 1) {
            carsService
                .listAllDeleted()
                .then(({data, count}) => {
                    res.status(200).json({
                        status: "OK",
                        data: {posts: data},
                    });
                })
                .catch((err) => {
                    res.status(400).json({
                        status: "FAIL",
                        message: err.message,
                    });
                });
        } else {
            res.status(401).json({
                status: "FAIL",
                message: "Unauthorized",
            });
        }
    },

    async createCar(req, res) {
        if (req.Users.id_type === 1 || req.Users.id_type === 2) {
            const {id_type, name_car, rent_cost, image_car} = req.body;
            carsService
                .create({id_type, name_car, rent_cost, image_car})
                .then((Car) => {
                    const id_car = Car.id;
                    const id_user = req.Users.id;
                    const activity = "Created a new car";
                    activityService.create({id_car, id_user, activity});
                    res.status(201).json({
                        status: "OK",
                        data: Car,
                        message: "Car Created",
                    });
                })
                .catch((err) => {
                    res.status(422).json({
                        status: "FAIL",
                        message: err.message,
                    });
                });
            console.log(carsService.id_car);
        } else {
            res.status(403).json({
                status: "FAIL",
                message: "You are not authorized to perform this action",
            });
        }
    },

    async updateCar(req, res) {
        if (req.Users.id_type === 1 || req.Users.id_type === 2) {
            carsService
                .update(req.params.id, req.body)
                .then(() => {
                    const id_car = req.params.id;
                    const id_user = req.Users.id;
                    const activity = "Update a car";
                    activityService.create({id_car, id_user, activity});
                    res.status(200).json({
                        status: "OK",
                        message: "Car Updated",
                    });
                })
                .catch((err) => {
                    res.status(422).json({
                        status: "FAIL",
                        message: err.message,
                    });
                });
        } else {
            res.status(403).json({
                status: "FAIL",
                message: "You are not authorized to perform this action",
            });
        }
    },

    async showCar(req, res) {
        if (req.Users.id_type === 1 || req.Users.id_type === 2) {
            carsService
                .get(req.params.id)
                .then((Car) => {
                    res.status(200).json({
                        status: "OK",
                        data: Car,
                    });
                })
                .catch((err) => {
                    res.status(422).json({
                        status: "FAIL",
                        message: err.message,
                    });
                });
        } else {
            res.status(403).json({
                status: "FAIL",
                message: "You are not authorized to perform this action",
            });
        }
    },

    async destroyCar(req, res) {
        if (req.Users.id_type === 1 || req.Users.id_type === 2) {
            carsService
                .delete(req.params.id)
                .then((Car) => {
                    const id_car = req.params.id;
                    const id_user = req.Users.id;
                    const activity = "Delete a car";
                    activityService.create({id_car, id_user, activity});
                    res.status(200).json({
                        status: "OK",
                        message: "Car Deleted",
                    });
                })
                .catch((err) => {
                    res.status(422).json({
                        status: "FAIL",
                        message: err.message,
                    });
                });
        } else {
            res.status(403).json({
                status: "FAIL",
                message: "You are not authorized to perform this action",
            });
        }
    },

    async listActivity(req, res) {
        if (req.Users.id_type === 1 || req.Users.id_type === 2) {
            activityService
                .list()
                .then((activity) => {
                    res.status(200).json({
                        status: "OK",
                        data: activity,
                    });
                })
                .catch((err) => {
                    res.status(422).json({
                        status: "FAIL",
                        message: err.message,
                    });
                });
        } else {
            res.status(403).json({
                status: "FAIL",
                message: "You are not authorized to perform this action",
            });
        }
    },
};
