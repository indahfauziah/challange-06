const type_userService = require("../../../services/type_usersService");

module.exports = {
    async list(req, res) {
        if (req.Users.id_type === 1) {
            type_userService
                .list()
                .then(({data}) => {
                    res.status(200).json({
                        status: "OK",
                        data: {data},
                    });
                })
                .catch((err) => {
                    res.status(400).json({
                        status: "FAIL",
                        message: err.message,
                    });
                });
        } else {
            res.status(403).json({
                status: "FAIL",
                message: "You are not authorized to perform this action",
            });
        }
    },

    async create(req, res) {
        if (req.Users.id_type === 1) {
            type_userService
                .create(req.body)
                .then((post) => {
                    res.status(201).json({
                        status: "OK",
                        data: post,
                    });
                })
                .catch((err) => {
                    res.status(422).json({
                        status: "FAIL",
                        message: err.message,
                    });
                });
        } else {
            res.status(403).json({
                status: "FAIL",
                message: "You are not authorized to perform this action",
            });
        }
    },

    async update(req, res) {
        if (req.Users.id_type === 1) {
            type_userService
                .update(req.params.id, req.body)
                .then(() => {
                    res.status(200).json({
                        status: "OK",
                        message: "Type User updated successfully",
                    });
                })
                .catch((err) => {
                    res.status(422).json({
                        status: "FAIL",
                        message: err.message,
                    });
                });
        } else {
            res.status(403).json({
                status: "FAIL",
                message: "You are not authorized to perform this action",
            });
        }
    },

    async show(req, res) {
        if (req.Users.id_type === 1) {
            type_userService
                .get(req.params.id)
                .then((post) => {
                    res.status(200).json({
                        status: "OK",
                        data: post,
                    });
                })
                .catch((err) => {
                    res.status(422).json({
                        status: "FAIL",
                        message: err.message,
                    });
                });
        } else {
            res.status(403).json({
                status: "FAIL",
                message: "You are not authorized to perform this action",
            });
        }
    },

    async destroy(req, res) {
        if (req.Users.id_type === 1) {
            type_userService
                .delete(req.params.id)
                .then(() => {
                    res.status(200).json({
                        status: "OK",
                        message: "Type user has been deleted",
                    });
                })
                .catch((err) => {
                    res.status(422).json({
                        status: "FAIL",
                        message: err.message,
                    });
                });
        } else {
            res.status(403).json({
                status: "FAIL",
                message: "You are not authorized to perform this action",
            });
        }
    },
};
