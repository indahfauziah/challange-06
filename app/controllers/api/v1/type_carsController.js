const type_carsService = require("../../../services/type_carsService");

module.exports = {
    async listTypeCar(req, res) {
        if (req.Users.id_type === 1 || req.Users.id_type === 2) {
            type_carsService
                .list()
                .then(({data}) => {
                    res.status(200).json({
                        status: "OK",
                        data: {data},
                    });
                })
                .catch((err) => {
                    res.status(400).json({
                        status: "FAIL",
                        message: err.message,
                    });
                });
        } else {
            res.status(403).json({
                status: "FAIL",
                message: "You are not authorized to perform this action",
            });
        }
    },

    async createTypeCar(req, res) {
        if (req.Users.id_type === 1 || req.Users.id_type === 2) {
            type_carsService
                .create(req.body)
                .then((post) => {
                    res.status(201).json({
                        status: "OK",
                        data: post,
                        message: "Type Car created successfully",
                    });
                })
                .catch((err) => {
                    res.status(422).json({
                        status: "FAIL",
                        message: err.message,
                    });
                });
        } else {
            res.status(403).json({
                status: "FAIL",
                message: "You are not authorized to perform this action",
            });
        }
    },

    async updateTypeCar(req, res) {
        if (req.Users.id_type === 1 || req.Users.id_type === 2) {
            type_carsService
                .update(req.params.id, req.body)
                .then(() => {
                    res.status(200).json({
                        status: "OK",
                        message: "Type Car updated successfully",
                    });
                })
                .catch((err) => {
                    res.status(422).json({
                        status: "FAIL",
                        message: err.message,
                    });
                });
        } else {
            res.status(403).json({
                status: "FAIL",
                message: "You are not authorized to perform this action",
            });
        }
    },

    async showTypeCar(req, res) {
        if (req.Users.id_type === 1 || req.Users.id_type === 2) {
            type_carsService
                .get(req.params.id)
                .then((data) => {
                    res.status(200).json({
                        status: "OK",
                        data: data,
                    });
                })
                .catch((err) => {
                    res.status(422).json({
                        status: "FAIL",
                        message: err.message,
                    });
                });
        } else {
            res.status(403).json({
                status: "FAIL",
                message: "You are not authorized to perform this action",
            });
        }
    },

    async destroyTypeCar(req, res) {
        if (req.Users.id_type === 1 || req.Users.id_type === 2) {
            type_carsService
                .delete(req.params.id)
                .then(() => {
                    res.status(200).json({
                        status: "OK",
                        message: "Type Car has been deleted",
                    });
                })
                .catch((err) => {
                    res.status(422).json({
                        status: "FAIL",
                        message: err.message,
                    });
                });
        } else {
            res.status(403).json({
                status: "FAIL",
                message: "You are not authorized to perform this action",
            });
        }
    },
};
