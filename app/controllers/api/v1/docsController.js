const yaml = require("yamljs"); 
const swaggerDocument = yaml.load("./openapi.yaml"); 

module.exports = {
    async getSwagger(req, res) {
        res.status(200).json(swaggerDocument);
    },
};
