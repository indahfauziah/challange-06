const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const {Users} = require("../../../models");
const SALT = 10;
const usersService = require("../../../services/usersService");

function encryptPassword(password) {
    return new Promise((resolve, reject) => {
        bcrypt.hash(password, SALT, (err, encryptedPassword) => {
            if (!!err) {
                reject(err);
                return;
            }

            resolve(encryptedPassword);
        });
    });
}

function checkPassword(encryptedPassword, password) {
    return new Promise((resolve, reject) => {
        bcrypt.compare(password, encryptedPassword, (err, isPasswordCorrect) => {
            if (!!err) {
                reject(err);
                return;
            }

            resolve(isPasswordCorrect);
        });
    });
}

// Create token function
function createToken(data) {
    return jwt.sign(data, process.env.JWT_SECRET || "secret", {
        expiresIn: 10 * 60,
    });
}

// Verify token function
function verifyToken(token) {
    return jwt.verify(token, process.env.JWT_SECRET || "secret");
}

module.exports = {
    async registerMember(req, res) {
        const email_user = req.body.email;
        const id_type = 3; 

        const password_user = await encryptPassword(req.body.password);
        if (req.body.password === "") {
            res.status(422).json({
                status: "FAIL",
                message: "Password is required",
            });
            return;
        }
        usersService
            .create({id_type, email_user, password_user})
            .then((post) => {
                res.status(201).json({
                    status: "OK",
                    data: post,
                });
            })
            .catch((err) => {
                res.status(422).json({
                    status: "FAIL",
                    message: err.message,
                });
            });
    },

    async registerAdmin(req, res) {
        if (req.Users.id_type === 1) {
            const email_user = req.body.email;
            const id_type = 2; 
            const password_user = await encryptPassword(req.body.password);
            if (req.body.password === "") {
                res.status(422).json({
                    status: "FAIL",
                    message: "Password is required",
                });
                return;
            }

            usersService
                .create({id_type, email_user, password_user})
                .then((post) => {
                    res.status(201).json({
                        status: "OK",
                        data: post,
                    });
                })
                .catch((err) => {
                    res.status(422).json({
                        status: "FAIL",
                        message: err.message,
                    });
                });
        } else {
            res.status(403).json({
                status: "FAIL",
                message: "You are not authorized to perform this action",
            });
        }
    },

    async login(req, res) {
        const email_user = req.body.email.toLowerCase(); 
        const password_user = req.body.password;

        const users = await Users.findOne({
            where: {email_user},
        });

        if (!users) {
            res.status(404).json({message: "Email tidak ditemukan"});
            return;
        }

        const isPasswordCorrect = await checkPassword(users.password_user, password_user);

        if (!isPasswordCorrect) {
            res.status(401).json({message: "Password salah!"});
            return;
        }

        // Create token
        const token = createToken({
            id: users.id,
            id_type: users.id_type,
            email: users.email,
            createdAt: users.createdAt,
            updatedAt: users.updatedAt,
        });

        // Return token and some information
        res.status(201).json({
            id: users.id,
            id_type: users.id_type,
            email: users.email,
            token,
            createdAt: users.createdAt,
            updatedAt: users.updatedAt,
        });
    },

    // Authorize middleware 
    async authorize(req, res, next) {
        try {
            const bearerToken = req.headers.authorization; 
            const token = bearerToken.split("Bearer ")[1]; 
            const tokenPayload = verifyToken(token); 

            req.Users = JSON.parse(JSON.stringify(await Users.findByPk(tokenPayload.id))); 
            delete req.Users.password_user; 

            next(); 
        } catch (error) {
            if (error.message.includes("jwt expired")) {
                res.status(401).json({message: "Token expired"});
                return;
            }
            res.status(401).json({
                message: "Unauthorized",
            });
        }
    },

    async whoAmI(req, res) {
        if (req.Users.id_type === 3) {
            res.status(200).json({
                status: "OK",
                data: req.Users,
                message: "You are a member",
            });
        } else if (req.Users.id_type === 2) {
            res.status(200).json({
                status: "OK",
                data: req.Users,
                message: "You are an admin",
            });
        } else if (req.Users.id_type === 1) {
            res.status(200).json({
                status: "OK",
                data: req.Users,
                message: "You are a super admin",
            });
        }
    },
};
