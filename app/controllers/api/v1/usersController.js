const userService = require("../../../services/usersService");

module.exports = {
    async list(req, res) {
        if (req.Users.id_type === 1) {
            userService
                .list()
                .then(({data}) => {
                    res.status(200).json({
                        status: "OK",
                        data: data,
                    });
                })
                .catch((err) => {
                    res.status(400).json({
                        status: "FAIL",
                        message: err.message,
                    });
                });
        } else {
            res.status(403).json({
                status: "FAIL",
                message: "You are not authorized to perform this action",
            });
        }
    },

    async updateToAdmin(req, res) {
        if (req.Users.id_type === 1) {
            const id_type = 2; // 2 = admin
            const id = req.params.id;
            const user = await userService.get(id);
            if (user.dataValues.id_type !== 2) {
                userService
                    .update(req.params.id, {id_type})
                    .then(() => {
                        res.status(200).json({
                            status: "OK",
                            message: "User has been updated to admin",
                        });
                    })
                    .catch((err) => {
                        res.status(422).json({
                            status: "FAIL",
                            message: err.message,
                        });
                    });
            } else {
                res.status(400).json({
                    status: "FAIL",
                    message: "User already is admin",
                });
            }
        } else {
            res.status(403).json({
                status: "FAIL",
                message: "You are not authorized to perform this action",
            });
        }
    },

    async updateToMember(req, res) {
        if (req.Users.id_type === 1) {
            const id_type = 3; // 3 = member
            const id = req.params.id;
            const user = await userService.get(id);
            if (user.dataValues.id_type !== 3) {
                userService
                    .update(req.params.id, {id_type})
                    .then(() => {
                        res.status(200).json({
                            status: "OK",
                            message: "User has been updated to member",
                        });
                    })
                    .catch((err) => {
                        res.status(422).json({
                            status: "FAIL",
                            message: err.message,
                        });
                    });
            } else {
                res.status(400).json({
                    status: "FAIL",
                    message: "User already is member",
                });
            }
        } else {
            res.status(403).json({
                status: "FAIL",
                message: "You are not authorized to perform this action",
            });
        }
    },

    async show(req, res) {
        if (req.Users.id_type === 1) {
            userService
                .get(req.params.id)
                .then((User) => {
                    res.status(200).json({
                        status: "OK",
                        data: User,
                    });
                })
                .catch((err) => {
                    res.status(422).json({
                        status: "FAIL",
                        message: err.message,
                    });
                });
        } else {
            res.status(403).json({
                status: "FAIL",
                message: "You are not authorized to perform this action",
            });
        }
    },

    async destroy(req, res) {
        if (req.Users.id_type === 1) {
            userService
                .delete(req.params.id)
                .then(() => {
                    res.status(200).json({
                        status: "OK",
                        message: "User has been deleted",
                    });
                })
                .catch((err) => {
                    res.status(422).json({
                        status: "FAIL",
                        message: err.message,
                    });
                });
        } else {
            res.status(403).json({
                status: "FAIL",
                message: "You are not authorized to perform this action",
            });
        }
    },
};
